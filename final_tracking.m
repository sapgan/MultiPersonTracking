function final_tracking(obj, event,vid,demoParameters)
    persistent handlesInput;
    tic
    cachedir = 'cache/';
    datadir = 'data/';
    frames_per_sec = demoParameters.framesPerTrigger;
%     vid_path = [cachedir 'images/'];
%     trigger(vid);
    images = cell(1,frames_per_sec);
    for i=1:frames_per_sec
        snapshot = getsnapshot(vid);
        images{i} = snapshot;
    end
    fname = [cachedir 'live_detec_res.mat'];
%     toc
    

      [dres bboxes] = detect_objects(images,demoParameters.numberCores);
    toc
    
      dres = build_graph(dres);
    
    c_en      = 10;     %% birth cost
    c_ex      = 10;     %% death cost
    c_ij      = 0;      %% transition cost
    betta     = 0.2;    %% betta
    max_it    = inf;    %% max number of iterations (max number of tracks)
    thr_cost  = 18;     %% max acceptable cost for a track (increase it to have more tracks.)
    

    dres_dp       = tracking_dp(dres, c_en, c_ex, c_ij, betta, thr_cost, max_it, demoParameters.useNMS);
    dres_dp.r     = -dres_dp.id;

    
    fnum = max(dres.fr);
    bboxes_tracked = dres2bboxes(dres_dp, fnum);
%     bboxes_tracked
%     time1 = tic;
    thr = -inf;
    load([datadir 'label_image_file']);
%     for i = 1:max(dres_dp.track_id)
%       bws(i).bw =  text_to_image(num2str(i), 20, 123);
%     end
%     save data/label_image_file bws
% 
%     load([datadir 'label_image_file']);
%     m=2;
%     for i=1:length(bws)                   %% adds some margin to the label images
%       [sz1 sz2] = size(bws(i).bw);
%       bws(i).bw = [zeros(sz1+2*m,m) [zeros(m,sz2); bws(i).bw; zeros(m,sz2)] zeros(sz1+2*m,m)];
%     end
    col = round((rand(3,1e4)/2+.5)*255);  %% we assume number of tracks is less than 1e4.
    for i=1:frames_per_sec
        bbox = bboxes_tracked(i).bbox
        im1 = images{1};
        if ~isempty(bbox)
            inds = find(bbox(:,end) > thr);
            im1 = show_bbox_on_image(im1, bbox(inds, :), bws, col);
            if isempty(handlesInput)
                handlesInput=imshow(im1);
            else
                set(handlesInput,'CData',im1);
            end
        end
    end
end
