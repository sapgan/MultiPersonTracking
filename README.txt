This work is inspired from the works of Hamed Pirsiavash, Deva Ramanan, Charless C. Fowlkes in "Globally-Optimal Greedy Algorithms for Tracking a Variable Number of Objects,".

The part based object detector is the matlab/C implementation from http://people.cs.uchicago.edu/~pff/latent/


In order to run tracking just run the 'camera_tracking' file in matlab. The program captures frames from the camera at regular intervals and then the 'final_tracking' function handles the frames by detecting the objects and then runnning the tracking alogrithm. The total number of frames to be displayed every seconds is the multiplication of number of triggers every second and no of captured frames on every trigger. The object detection  is done in parallel and so number of cores in the machine should be set in the 'set_demo_params' file. For the tracking Non-Maxima Suppression can be used by setting the 'useNMS' in 'set_demo_params' to 1.

In order to run the code do the following:
 	1) Set the id of the camera from which to capture in the file 'set_demo_params.m' using the variable 'videoInputId' 
 	2) Set whether to use Non-maxima Suppression or not by setting the variable 'useNMS' in 'set_demo_params.m'
 	3) Set the no of cpu cores to use in 'numberCores' in 'set_demo_params.m'
 	4) Set the number of triggers every second in the variable 'triggersPerSec' in 'set_demo_params.m'
 	5) Set the number of frames every trigger in the variable 'framesPerTrigger' in 'set_demo_params.m'
 	6) Run 'camera_tracking'


P.S:	1) If the program says it can't find some system files, run 'addlocalpath' to add the system files path to matlab session.
		2) In case of errors in between or exiting before the entire program runs, run 'clear_all' to clear all left-over variables.