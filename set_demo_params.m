%function to create demo parameters
clear demoParameters;

demoParameters.fileName = 'office';
demoParameters.videoInputName = 'macvideo';
demoParameters.videoInputId = 2;
demoParameters.numberCores = 4;
demoParameters.useNMS =1;
demoParameters.triggersPerSec = 4;
demoParameters.framesPerTrigger = 4;
save('demo_parameters.mat','demoParameters');