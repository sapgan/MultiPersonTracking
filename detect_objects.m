function [dres bboxes] = detect_objects(images,cores_used)
thresh = -2;              %% threshod on SVM response in human detection, we'll have more detections by decreasing it.

n_cores = cores_used;              %% number of cores to use. Matlab doesn't let you use more that 8 cores on a single machine. decrese it if you have less than 8 cores.
n_cores = min(8, n_cores);
poolobj = gcp('nocreate');
if isempty(poolobj)
    poolsize = 0;
else
    poolsize = poolobj.NumWorkers;
end
if poolsize ~= n_cores
  if poolsize > 0
    delete(gcp);
  end
  parpool(n_cores);
end


tmp = load ('3rd_party/voc-release3.1/INRIA/inria_final.mat');  %% load the model for human. This can be changed to any of those 20 objects in PASCAL competition.
model= tmp.model;
clear tmp

% tic
parfor i=1:length(images)
  display(['frame ' num2str(i)]);
  im = images{i};
  im = imresize(im,0.3);                %% decrease the image resolution to make algorithm faster.
  
  boxes = detect(im, model, thresh);  %% running the detector
  bbox =  getboxes(model, boxes);
  
  bboxes(i).bbox = nms(bbox, 0.5);    %% running non-max-suppression to suppress overlaping weak detections.
end
% toc
dres = bboxes2dres(bboxes);           %% converting the data format.
dres.x = dres.x/0.3;                    %% compensate reducing image size.
dres.y = dres.y/0.3;
dres.w = dres.w/0.3;
dres.h = dres.h/0.3;


