%% Camera Tracking
%
% Running the video tracker on live video
%
% Saptarshi Gan, 2016
%

close all;
clear all;
addpath('3rd_party/voc-release3.1/');           %% this code is downloaded from http://people.cs.uchicago.edu/~pff/latent/
cd ('3rd_party/voc-release3.1')
compile                                         %% compiling mex files for part-based object detector
cd('../..');


load('demo_parameters.mat');

hFigure = figure;


TriggersPerSecond = demoParameters.triggersPerSec;
accumulateImages = 50;

%initialise video feed
vidObj = videoinput(demoParameters.videoInputName,demoParameters.videoInputId);
set(vidObj,'FramesPerTrigger',demoParameters.framesPerTrigger);
% Go on forever until stopped
set(vidObj,'TriggerRepeat',Inf);
% Get a grayscale image
set(vidObj,'ReturnedColorSpace','rgb');
triggerconfig(vidObj, 'Manual');

% set up timer object
TimerData=timer('TimerFcn', {@final_tracking,vidObj,demoParameters},'Period',1/TriggersPerSecond,...
    'ExecutionMode','fixedRate','BusyMode','drop')

% Start video and timer object
start(vidObj);
start(TimerData);

% We go on until the figure is closed
uiwait(hFigure);

% Clean up everything
stop(TimerData);
delete(TimerData);
stop(vidObj);
delete(vidObj);
% clear persistent variables
clear functions;
